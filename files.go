package woodstock

import (
	"encoding/json"
	"io"
	"net/url"
)

// FileResult response definition
type FileResult struct {
	*CommonResponse
	Data File `json:"data"`
}

// FilesResult response definition
type FilesResult struct {
	*CommonResponse
	Data []File `json:"data"`
}

// FileDetails object definition
type FileDetails struct {
	Name     string `json:"name"`
	IsPublic bool   `json:"is_public"`
}

// GetFile retrieve a file object
// https://pnut.io/docs/resources/files/lookup#get-files-id
func (c *Client) GetFile(id string, qparams url.Values) (result FileResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: FileAPI + "/" + id + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetFiles retrieve multiple file objects
// https://pnut.io/docs/resources/files/lookup#get-files
func (c *Client) GetFiles(qparams url.Values) (result FilesResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: FileAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetUserFiles retrieve the authenticated users file objects
// https://pnut.io/docs/resources/files/lookup#get-users-me-files
func (c *Client) GetUserFiles(qparams url.Values) (result FilesResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserMeAPI + "/files?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// CreateFile upload a complete file
// https://pnut.io/docs/resources/files/lifecycle#post-files
func (c *Client) CreateFile(params map[string]string, reader io.Reader) (result FileResult, err error) {
	// v.Set("field", "content")
	params["field"] = "content"
	responseCh := make(chan response)
	c.queryQueue <- query{url: FileAPI, params: params, reader: reader, data: &result, method: "POST", responseCh: responseCh}
	return result, (<-responseCh).err
}

// CreateFilePlaceholder create a file placeholder
// https://pnut.io/docs/resources/files/lifecycle#post-files
func (c *Client) CreateFilePlaceholder(params File) (result FileResult, err error) {
	json, err := json.Marshal(params)
	if err != nil {
		return
	}
	responseCh := make(chan response)
	c.queryQueue <- query{url: FileAPI, data: &result, method: "POST", json: string(json), responseCh: responseCh}
	return result, (<-responseCh).err
}

// TODO: is broken
// UpdateFileContent uploads a file for an existing file placeholder
// https://pnut.io/docs/resources/files/lifecycle#put-files-id-content
// func (c *Client) UpdateFileContent(id string, reader io.Reader) (result FileResult, err error) {
// 	responseCh := make(chan response)
// 	c.queryQueue <- query{url: FileAPI + "/" + id + "/content", reader: reader, data: &result, method: "PUT", responseCh: responseCh}
// 	return result, (<-responseCh).err
// }

// UpdateFileDetails updates a files details, only name, is_public, and raw
// https://pnut.io/docs/resources/files/lifecycle#put-files-id
func (c *Client) UpdateFileDetails(id string, details FileDetails) (result FileResult, err error) {
	json, err := json.Marshal(details)
	if err != nil {
		return
	}
	responseCh := make(chan response)
	c.queryQueue <- query{url: FileAPI + "/" + id, data: &result, method: "PUT", json: string(json), responseCh: responseCh}
	return result, (<-responseCh).err
}

// DeleteFile delete a file. This will not disassociate a file with any other objects (posts, messages...)
// https://pnut.io/docs/resources/files/lifecycle#delete-files-id
func (c *Client) DeleteFile(id string) (result FileResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: FileAPI + "/" + id, data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}
