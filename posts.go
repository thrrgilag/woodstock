package woodstock

import (
	"encoding/json"
	"net/url"
	"strings"
)

// PostResult response definition
type PostResult struct {
	*CommonResponse
	Data Post `json:"data"`
}

// PostsResult response definition
type PostsResult struct {
	*CommonResponse
	Data []Post `json:"data"`
}

// ActionsResult response definition
type ActionsResult struct {
	*CommonResponse
	Data []Action `json:"data"`
}

// NewPost object definition
type NewPost struct {
	Text               string      `json:"text"`
	ParseLinks         string      `json:"entities.parse_links,omitempty"`
	ParseMarkdownLinks string      `json:"entities.parse_markdown_links,omitempty"`
	Nsfw               string      `json:"is_nsfw,omitempty"`
	Raw                interface{} `json:"raw"`
	ReplyTo            string      `json:"reply_to,omitempty"`
}

// GetPost retrieves a post
// https://pnut.io/docs/resources/posts/lookup#get-posts-id
func (c *Client) GetPost(id string, qparams url.Values) (result PostResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "/" + id + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetPosts retrieves mutliple posts
// https://pnut.io/docs/resources/posts/lookup#get-posts
func (c *Client) GetPosts(ids []string, qparams url.Values) (result PostsResult, err error) {
	qparams.Set("ids", strings.Join(ids, ","))
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetPostRevisions retrieves a list of reviesions of a post
// https://pnut.io/docs/resources/posts/lookup#get-posts-id-revisions
func (c *Client) GetPostRevisions(id string, qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "/" + id + "/revisions?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// Post creates a post
// https://pnut.io/docs/resources/posts/lifecycle#post-posts
func (c *Client) Post(params NewPost, qparams url.Values) (result PostResult, err error) {
	json, err := json.Marshal(params)
	if err != nil {
		return
	}
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "?" + qparams.Encode(), data: &result, method: "POST", responseCh: responseCh, json: string(json)}
	return result, (<-responseCh).err
}

// RevisePost updates an existing post
// https://pnut.io/docs/resources/posts/lifecycle#put-posts-id
func (c *Client) RevisePost(id string, params NewPost) (result PostResult, err error) {
	json, err := json.Marshal(params)
	if err != nil {
		return
	}
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "/" + id, data: &result, method: "PUT", responseCh: responseCh, json: string(json)}
	return result, (<-responseCh).err
}

// DeletePost deletes a post
// https://pnut.io/docs/resources/posts/lifecycle#delete-posts-id
func (c *Client) DeletePost(id string) (result PostResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "/" + id, data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// MeStream retrieves a personal stream
// https://pnut.io/docs/resources/posts/streams#get-posts-streams-me
func (c *Client) MeStream(qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: StreamMeAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// UnifiedStream retrieves a unified stream
// https://pnut.io/docs/resources/posts/streams#get-posts-streams-unified
func (c *Client) UnifiedStream(qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: StreamUnifiedAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GlobalStream retrieves the global stream
// https://pnut.io/docs/resources/posts/streams#get-posts-streams-unified
func (c *Client) GlobalStream(qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: StreamGlobalAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// TagStream retrieves a stream for a given tag
// https://pnut.io/docs/resources/posts/streams#get-posts-tag-tag
func (c *Client) TagStream(tag string, qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: StreamTabBaseURL + "/" + tag + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetThread retrieves post in a given thread
// https://pnut.io/docs/resources/posts/threads#get-posts-id-thread
func (c *Client) GetThread(id string, qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "/" + id + "/thread?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// Repost repeats a post
// https://pnut.io/docs/resources/posts/reposts#put-posts-id-repost
func (c *Client) Repost(id string) (result PostResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "/" + id + "/repost", data: &result, method: "PUT", responseCh: responseCh}
	return result, (<-responseCh).err
}

// UnRepost removes a repeated post
// https://pnut.io/docs/resources/posts/reposts#delete-posts-id-repost
func (c *Client) UnRepost(id string) (result PostResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "/" + id + "/repost", data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// Bookmark creates a bookmark (stars) a post
// https://pnut.io/docs/resources/posts/bookmarks#put-posts-id-bookmark
func (c *Client) Bookmark(id string) (result PostResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "/" + id + "/bookmark", data: &result, method: "PUT", responseCh: responseCh}
	return result, (<-responseCh).err
}

// UnBookmark deletes a bookmark (unstars) a post
// https://pnut.io/docs/resources/posts/bookmarks#put-posts-id-bookmark
func (c *Client) UnBookmark(id string) (result PostResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "/" + id + "/bookmark", data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetActions retrieves actions
// https://pnut.io/docs/resources/posts/interactions#get-posts-id-interactions
func (c *Client) GetActions(id string, qparams url.Values) (result ActionsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: PostAPI + "/" + id + "/interactions?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}
