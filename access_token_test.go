package woodstock

import (
	"os"
	"testing"
	"time"
)

const Delay = 3 * time.Second

type Config struct {
	ClientID    string
	AccessToken string
}

func GetConfig() (config Config, err error) {
	config.ClientID = os.Getenv("PNUT_CLIENT_ID")
	config.AccessToken = os.Getenv("PNUT_ACCESS_TOKEN")
	return
}

func TestAccessToken(t *testing.T) {
	// TODO sort out a proper set of auth tests
	t.Logf("skipping for now...")
}
