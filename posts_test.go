package woodstock

import (
	"net/url"
	"strconv"
	"testing"
	"time"
)

func TestPost(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	text := "Hello pnut.io"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)

	language := map[string]string{"language": "en"}
	raw := map[string][]map[string]string{
		"io.pnut.core.language": []map[string]string{language},
	}

	newpost := NewPost{Text: text, Raw: raw}
	post, err := client.Post(newpost, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if post.Data.Content.Text != text {
		t.Errorf("Post appears incorrect, got: %s, want: %s", post.Data.Content.Text, text)
	}
	time.Sleep(Delay)
}

func TestReplyPost(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	text := "Hello pnut.io reply"
	replyTo := "18813"

	language := map[string]string{"language": "en"}
	raw := map[string][]map[string]string{
		"io.pnut.core.language": []map[string]string{language},
	}

	newpost := NewPost{Text: text, ReplyTo: replyTo, Raw: raw}
	post, err := client.Post(newpost, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if post.Data.Content.Text != text {
		t.Errorf("Post appears incorrect, got: %s, want: %s", post.Data.Content.Text, text)
	}
	time.Sleep(Delay)
}

func TestLongPost(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	text := "Hello pnut.io"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)

	language := map[string]string{"language": "en"}
	longpost := map[string]string{}
	longpost["body"] = text
	longpost["title"] = "longpost test"
	longpost["tstamp"] = strconv.FormatInt(time.Now().UnixNano()/int64(time.Microsecond), 10)

	raw := map[string][]map[string]string{
		"io.pnut.core.language": []map[string]string{language},
		"nl.chimpnut.blog.post": []map[string]string{longpost},
	}

	newpost := NewPost{Text: text, Raw: raw}
	post, err := client.Post(newpost, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if post.Data.Content.Text != text {
		t.Errorf("Post appears incorrect, got: %s, want: %s", post.Data.Content.Text, text)
	}
	time.Sleep(Delay)
}

func TestPhotoPost(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	text := "I don't always have a slow internet connection."
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	filevalue := map[string]string{}
	filevalue["file_id"] = "14984"
	filevalue["file_token"] = "5hwO-VFP-IAjai_yq2MwJ3PXvw__D6vr"
	filevalue["format"] = "oembed"
	value := map[string]map[string]string{"+io.pnut.core.file": filevalue}
	raw := map[string][]map[string]map[string]string{
		"io.pnut.core.oembed": []map[string]map[string]string{value},
	}

	newpost := NewPost{Text: text, Raw: raw}
	post, err := client.Post(newpost, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if post.Data.Content.Text != text {
		t.Errorf("Post appears incorrect, got: %s, want: %s", post.Data.Content.Text, text)
	}
	time.Sleep(Delay)
}

func TestGetPost(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	postID := "541893"
	text := "Hello pnut.io"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("include_message_raw", "1")
	post, err := client.GetPost(postID, params)
	if err != nil {
		t.Error(err)
	}
	if post.Data.Content.Text != text {
		t.Errorf("Post appears incorrect, got: %s, want: %s", post.Data.Content.Text, text)
	}
	time.Sleep(Delay)
}

func TestGetPosts(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	postIDs := []string{"1", "2", "3"}
	text1 := "So, West Virginia--my birth state--they changed the slogan on the interstate signs to \"Open for business\" for a while. -Ross\n\n#quoteSunday"
	text2 := "posting test"
	text3 := "Hello World\n"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("include_message_raw", "1")
	posts, err := client.GetPosts(postIDs, params)
	if err != nil {
		t.Error(err)
	}
	if posts.Data[0].Content.Text != text1 {
		t.Errorf("Post appears incorrect, got: %s, want: %s", posts.Data[0].Content.Text, text1)
	}
	if posts.Data[1].Content.Text != text2 {
		t.Errorf("Post appears incorrect, got: %s, want: %s", posts.Data[1].Content.Text, text2)
	}
	if posts.Data[2].Content.Text != text3 {
		t.Errorf("Post appears incorrect, got: %s, want: %s", posts.Data[2].Content.Text, text3)
	}
	time.Sleep(Delay)
}

func TestGetPostRevisions(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	postID := "544502"
	text := "Blarp"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("include_message_raw", "1")
	posts, err := client.GetPostRevisions(postID, params)
	if err != nil {
		t.Error(err)
	}
	if posts.Data[0].Content.Text != text {
		t.Errorf("Post appears incorrect, got: %s, want: %s", posts.Data[0].Content.Text, text)
	}
	time.Sleep(Delay)
}

func TestRevisePost(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	text := "Hello pnut.io."
	newtext := "Hello pnut.io golang test"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	newpost := NewPost{Text: text}
	post, err := client.Post(newpost, url.Values{})
	if err != nil {
		t.Error(err)
	}
	newpost = NewPost{Text: newtext}
	rpost, rerr := client.RevisePost(post.Data.ID, newpost)
	if rerr != nil {
		t.Error(rerr)
	}
	if rpost.Data.Content.Text != newtext {
		t.Errorf("Revised post appears incorrect, got: %s, want: %s", post.Data.Content.Text, newtext)
	}
	time.Sleep(Delay)
}

func TestDeletePost(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	text := "Hello pnut.io_"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	newpost := NewPost{Text: text}
	post, err := client.Post(newpost, url.Values{})
	if err != nil {
		t.Error(err)
	}
	dpost, derr := client.DeletePost(post.Data.ID)
	if derr != nil {
		t.Error(derr)
	}
	if dpost.Data.IsDeleted != true {
		t.Errorf("Delete post appears to have failed")
	}
	time.Sleep(Delay)
}

func TestMeStream(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("count", "5")
	posts, err := client.MeStream(params)
	if err != nil {
		t.Error(err)
	}
	if len(posts.Data) != 5 {
		t.Errorf("Incorrect number of posts fetched, got: %d, want: %d", len(posts.Data), 5)
	}
	time.Sleep(Delay)
}

func TestUnifiedStream(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("count", "5")
	posts, err := client.UnifiedStream(params)
	if err != nil {
		t.Error(err)
	}
	if len(posts.Data) != 5 {
		t.Errorf("Incorrect number of posts fetched, got: %d, want: %d", len(posts.Data), 5)
	}
	time.Sleep(Delay)
}

func TestGlobalStream(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("count", "5")
	posts, err := client.GlobalStream(params)
	if err != nil {
		t.Error(err)
	}
	if len(posts.Data) != 5 {
		t.Errorf("Incorrect number of posts fetched, got: %d, want: %d", len(posts.Data), 5)
	}
	time.Sleep(Delay)
}

func TestTagStream(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	tag := "golang"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("count", "5")
	posts, err := client.TagStream(tag, params)
	if err != nil {
		t.Error(err)
	}
	if len(posts.Data) != 5 {
		t.Errorf("Incorrect number of posts fetched, got: %d, want: %d", len(posts.Data), 5)
	}
	time.Sleep(Delay)
}

func TestRepost(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	postID := "257434"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	post, err := client.Repost(postID)
	if err != nil {
		t.Error(err)
	}
	if post.Data.YouReposted != true {
		t.Errorf("Post does not show as reposted.")
	}
	time.Sleep(Delay)
}

func TestUnRepost(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	postID := "257434"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	post, err := client.UnRepost(postID)
	if err != nil {
		t.Error(err)
	}
	if post.Data.YouReposted != false {
		t.Errorf("Post does show as reposted.")
	}
	time.Sleep(Delay)
}

func TestBookmark(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	postID := "257434"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	post, err := client.Bookmark(postID)
	if err != nil {
		t.Error(err)
	}
	if post.Data.YouBookmarked != true {
		t.Errorf("Post does not show as bookmarked: %t", post.Data.YouBookmarked)
	}
	time.Sleep(Delay)
}

func TestUnBookmark(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	postID := "257434"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	post, err := client.UnBookmark(postID)
	if err != nil {
		t.Error(err)
	}
	if post.Data.YouBookmarked != false {
		t.Errorf("Post still shows as bookmarked")
	}
	time.Sleep(Delay)
}

func TestGetActions(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	postID := "1"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	posts, err := client.GetActions(postID, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if len(posts.Data) < 1 {
		t.Errorf("There does not appear to be any actions on post.")
	}
	time.Sleep(Delay)
}
