package main

import (
	"fmt"
	"net/url"

	"mcmillian.dev/go/woodstock"
)

const (
	CLIENT_ID     = "client id"
	CLIENT_SECRET = "client secret"
	ACCESS_TOKEN  = "access token"
)

func main() {
	client := woodstock.NewClient(CLIENT_ID, CLIENT_SECRET)
	client.SetAccessToken(ACCESS_TOKEN)
	v := url.Values{}
	v.Set("text", "Hello pnut.io")
	_, err := client.Post(v)
	if err != nil {
		fmt.Println(err)
		return
	}
}
