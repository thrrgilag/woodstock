package woodstock

import (
	"net/http"
)

// pnut.io v0 API endpoints, see https://pnut.io/docs/implementation/overview
const (
	AuthenticateURL        = "https://pnut.io/oauth/authenticate"
	APIBasesURL            = "https://api.pnut.io/v1/"
	OAuthAccessTokenAPI    = APIBasesURL + "oauth/access_token"
	PostAPI                = APIBasesURL + "posts"
	StreamBaseURL          = PostAPI + "/" + "streams"
	StreamMeAPI            = StreamBaseURL + "/me"
	StreamUnifiedAPI       = StreamBaseURL + "/unified"
	StreamGlobalAPI        = StreamBaseURL + "/global"
	StreamTabBaseURL       = PostAPI + "/" + "tag"
	UserAPI                = APIBasesURL + "users"
	UserMeAPI              = UserAPI + "/me"
	MeChannelsBaseURL      = UserMeAPI + "/channels"
	UnreadChannelNumberAPI = MeChannelsBaseURL + "/num_unread"
	SubscribedChannelsAPI  = MeChannelsBaseURL + "/subscribed"
	MutedChannelsAPI       = MeChannelsBaseURL + "/muted"
	MeMessagesAPI          = UserMeAPI + "/messages"
	ChannelAPI             = APIBasesURL + "channels"
	PresenceAPI            = APIBasesURL + "presence"
	ClientAPI              = APIBasesURL + "clients"
	MarkerAPI              = APIBasesURL + "markers"
	FileAPI                = APIBasesURL + "files"
)

// API definition
type API struct {
	accessToken string
	HTTPClient  *http.Client
}
