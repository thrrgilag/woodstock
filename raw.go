package woodstock

// RawEmbed object definition common across other embed objects
type RawEmbed struct {
	Version string `json:"version"`
	Type    string `json:"type"`
}

// PhotoEmbed object definition
type PhotoEmbed struct {
	*RawEmbed
	Width         int    `json:"width"`
	Height        int    `json:"height"`
	Title         string `json:"title"`
	URL           string `json:"url"`
	AuthorName    string `json:"author_name"`
	AuthorURL     string `json:"author_url"`
	ProviderName  string `json:"provider_name"`
	ProviderURL   string `json:"provider_url"`
	EmbeddableURL string `json:"embeddable_url"`
}

// VideoEmbed object definition
type VideoEmbed struct {
	*PhotoEmbed
	HTML string `json:"html"`
}

// HTML5VideoEmbed object definition
type HTML5VideoEmbed struct {
	*PhotoEmbed
	Sources []VideoSource `json:"sources"`
}

// VideoSource object definition
type VideoSource struct {
	Type string `json:"type"`
	URL  string `json:"url"`
}

// AudioEmbed object definition
type AudioEmbed struct {
	*RawEmbed
	Genere        string `json:"genere"`
	License       string `json:"license"`
	Release       string `json:"release"`
	TrackType     string `json:"track_type"`
	EmbeddableURL string `json:"embeddable_url"`
	Duration      int    `json:"duration"`
	Bitrate       int    `json:"bitrate"`
	Title         string `json:"title"`
	ProviderName  string `json:"provider_name"`
	ProviderURL   string `json:"provider_url"`
	FileID        string `json:"file_id"`
	FileTokenRead string `json:"file_token_read"`
	URLExpiresAt  string `json:"url_expires_at"`
	URL           string `json:"url"`
}
