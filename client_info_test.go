package woodstock

import (
	"testing"
	"time"
)

func TestGetClient(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	idstring := "fcYLbaFw47KMtMGZ9OBn7CXFLtxAkqDB"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	res, err := client.GetClient(idstring)
	if err != nil {
		t.Error(err)
	}
	t.Log(res.Meta)
	time.Sleep(Delay)
}
