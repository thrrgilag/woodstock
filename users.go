package woodstock

import (
	"io"
	"net/url"
	"strings"
)

// UserResult user response object
type UserResult struct {
	*CommonResponse
	Data User `json:"data"`
}

// UsersResult response object
type UsersResult struct {
	*CommonResponse
	Data []User `json:"data"`
}

// StringResult resposne object
type StringResult struct {
	Data string `json:"data"`
}

// PresenceResult response object
type PresenceResult struct {
	*CommonResponse
	Data Presence `json:"data"`
}

// NumberResult response object
type NumberResult struct {
	*CommonResponse
	Data map[string]int `json:"data"`
}

// MessagesResult response object
type MessagesResult struct {
	*CommonResponse
	Data []Message `json:"data"`
}

// ClientInfosResult response object
type ClientInfosResult struct {
	*CommonResponse
	Data []ClientInfo `json:"data"`
}

// GetUser retrieves a user
// https://pnut.io/docs/resources/users/lookup#get-users-id
func (c *Client) GetUser(id string, qparams url.Values) (result UserResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetUsers retrieves multiple users
// https://pnut.io/docs/resources/users/lookup#get-users
func (c *Client) GetUsers(ids []string, qparams url.Values) (result UsersResult, err error) {
	responseCh := make(chan response)
	qparams.Set("ids", strings.Join(ids, ","))
	c.queryQueue <- query{url: UserAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// ReplaceProfile replaces a user profile
// this func will be updated
// https://pnut.io/docs/resources/users/profile#put-users-me
func (c *Client) ReplaceProfile(json string) (result UserResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserMeAPI, data: &result, method: "PUT", responseCh: responseCh, json: json}
	return result, (<-responseCh).err
}

// UpdateProfile updates a user profile
// this func will be updated
// https://pnut.io/docs/resources/users/profile#patch-users-me
func (c *Client) UpdateProfile(json string) (result UserResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserMeAPI, data: &result, method: "PATCH", responseCh: responseCh, json: json}
	return result, (<-responseCh).err
}

// GetAvatarURL retrieves a users avatar URL
// https://pnut.io/docs/resources/users/profile#get-users-id-avatar
func (c *Client) GetAvatarURL(id string, qparams url.Values) (url string, err error) {
	result := &StringResult{}
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/avatar?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh, redirect: true}
	return result.Data, (<-responseCh).err
}

// UploadAvatar updates a user avatar
// https://pnut.io/docs/resources/users/profile#post-users-me-avatar
func (c *Client) UploadAvatar(params map[string]string, reader io.Reader) (result UserResult, err error) {
	params["field"] = "avatar"
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserMeAPI + "/avatar", params: params, reader: reader, data: &result, method: "POST", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetCoverURL retireves a cover URL
// https://pnut.io/docs/resources/users/profile#get-users-id-cover
func (c *Client) GetCoverURL(id string, qparams url.Values) (url string, err error) {
	result := &StringResult{}
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/cover?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh, redirect: true}
	return result.Data, (<-responseCh).err
}

// UploadCover updates a cover image
// https://pnut.io/docs/resources/users/profile#post-users-me-cover
func (c *Client) UploadCover(params map[string]string, reader io.Reader) (result UserResult, err error) {
	params["field"] = "cover"
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserMeAPI + "/cover", params: params, reader: reader, data: &result, method: "POST", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetFollowing retrieve following list
// https://pnut.io/docs/resources/users/following#get-users-id-following
func (c *Client) GetFollowing(id string, qparams url.Values) (result UsersResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/following?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetFollowers retrieve followers list
// https://pnut.io/docs/resources/users/followers#get-users-id-followers
func (c *Client) GetFollowers(id string, qparams url.Values) (result UsersResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/followers?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// Follow a user
// https://pnut.io/docs/resources/users/following#put-users-id-follow
func (c *Client) Follow(id string) (result UserResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/follow", data: &result, method: "PUT", responseCh: responseCh}
	return result, (<-responseCh).err
}

// UnFollow a user
// https://pnut.io/docs/resources/users/following#delete-users-id-follow
func (c *Client) UnFollow(id string) (result UserResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/follow", data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetMuted retrieve muted users
// https://pnut.io/docs/resources/users/muting#get-users-id-muted
func (c *Client) GetMuted(qparams url.Values) (result UsersResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserMeAPI + "/muted?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// Mute a user
// https://pnut.io/docs/resources/users/muteing#put-users-id-mute
func (c *Client) Mute(id string) (result UserResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/mute", data: &result, method: "PUT", responseCh: responseCh}
	return result, (<-responseCh).err
}

// UnMute a user
// https://pnut.io/docs/resources/users/muting#delete-users-id-mute
func (c *Client) UnMute(id string) (result UserResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/mute", data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetBlocked retrieve blocked users
// https://pnut.io/docs/resources/users/muting#get-users-id-blocked
func (c *Client) GetBlocked(qparams url.Values) (result UsersResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserMeAPI + "/blocked?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// Block a user
// https://pnut.io/docs/resources/users/blocking#put-users-id-block
func (c *Client) Block(id string) (result UserResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/block", data: &result, method: "PUT", responseCh: responseCh}
	return result, (<-responseCh).err
}

// UnBlock a user
// https://pnut.io/docs/resources/posts/bookmarks#delete-posts-id-bookmark
func (c *Client) UnBlock(id string) (result UserResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/block", data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetPresence retrieve presence user
// https://pnut.io/docs/resources/users/presence#get-users-id-presence
func (c *Client) GetPresence(id string) (result PresenceResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/presence", data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// SetPresence for user
// https://pnut.io/docs/resources/users/presence#put-users-id-presence
func (c *Client) SetPresence(presence string) (result PresenceResult, err error) {
	v := url.Values{}
	v.Set("presence", presence)
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/me/presence", form: v, data: &result, method: "PUT", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetMentions retrieves mentions
// https://pnut.io/docs/resources/posts/streams#get-users-id-mentions
func (c *Client) GetMentions(id string, qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/mentions?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetPostsFromUser retrieve posts from a user
// https://pnut.io/docs/resources/posts/streams#get-users-id-posts
func (c *Client) GetPostsFromUser(id string, qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/posts?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetBookmarks retrieve a users bookmarks
// https://pnut.io/docs/resources/posts/bookmarks#get-users-id-bookmarks
func (c *Client) GetBookmarks(id string, qparams url.Values) (result PostsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/bookmarks?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetActionsForMe retireve a users actions
// https://pnut.io/docs/resources/users/interactions#get-users-me-interactions
func (c *Client) GetActionsForMe(qparams url.Values) (result ActionsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserMeAPI + "/interactions?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetChannelsByMe retireves channels created by user
// https://pnut.io/docs/resources/channels/lookup#get-users-me-channels
func (c *Client) GetChannelsByMe(qparams url.Values) (result ChannelsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserMeAPI + "/channels?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetNumberOfUnreadChannels retireves the number of unread private messages
// https://pnut.io/docs/resources/channels/lookup#get-users-me-channels-num_unread-pm
func (c *Client) GetNumberOfUnreadChannels(qparams url.Values) (result NumberResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UnreadChannelNumberAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// MarkReadChannels marks private messages as read
// https://pnut.io/docs/resources/channels/lookup#delete-users-me-channels-num_unread-pm
func (c *Client) MarkReadChannels(chantypes []string) (result NumberResult, err error) {
	v := url.Values{}
	v.Set("channel_types", strings.Join(chantypes, ","))
	responseCh := make(chan response)
	c.queryQueue <- query{url: UnreadChannelNumberAPI + "?" + v.Encode(), data: &result, method: "DELETE", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetSubscribedChannels retrieves list of subscribed channels
// https://pnut.io/docs/resources/channels/subscribing#get-users-me-channels-subscribed
func (c *Client) GetSubscribedChannels(qparams url.Values) (result ChannelsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: SubscribedChannelsAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetMutedChannels retrieves list of muted channels
// https://pnut.io/docs/resources/channels/muting#get-users-me-channels-muted
func (c *Client) GetMutedChannels(qparams url.Values) (result ChannelsResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: MutedChannelsAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetMessagesByMe retireves messages created by user
// https://pnut.io/docs/resources/messages/lookup#get-users-me-messages
func (c *Client) GetMessagesByMe(qparams url.Values) (result MessagesResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: MeMessagesAPI + "?" + qparams.Encode(), data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}

// GetClients retrieves client information
// https://pnut.io/docs/resources/clients#get-users-id-clients
func (c *Client) GetClients(id string) (result ClientInfosResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: UserAPI + "/" + id + "/clients", data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}
