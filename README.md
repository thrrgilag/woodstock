# woodstock

A client library for pnut.io written in Go

[source code] -
[issue tracker] -
[releases]

## Installation

```sh
go get mcmillian.dev/go/woodstock
```

## Examples

New post

```go
package main

import (
    "fmt"
    "net/url"
    "mcmillian.dev/go/woodstock"
)

func main() {
    client := woodstock.NewClient("<ClientID>","")
    client.SetAccessToken("<AccessToken>")
    text := "Hello pnut.io"
    v := url.Values{}
    v.Set("text", text)
    post, err := client.Post(v)
    if err != nil {
        fmt.Println(err)
    }
    fmt.Println(post.Data.Content.Text)
}
```

## Contributing

You can open issues and merge requests to this project on [GitLab].


[source code]: https://gitlab.com/thrrgilag/woodstock
[issue tracker]: https://gitlab.com/thrrgilag/woodstock/-/issues
[releases]: https://gitlab.com/thrrgilag/woodstock/-/releases
[GitLab]: https://gitlab.com/thrrgilag/woodstock
