package woodstock

// PresencesResult response object
type PresencesResult struct {
	*CommonResponse
	Data []Presence `json:"data"`
}

// GetPresences retrieves precense
// https://pnut.io/docs/resources/users/presence#get-presence
func (c *Client) GetPresences() (result PresencesResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: PresenceAPI, data: &result, method: "GET", responseCh: responseCh}
	return result, (<-responseCh).err
}
