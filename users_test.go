package woodstock

import (
	"net/url"
	"os"
	"testing"
	"time"
)

func TestGetUser(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "518"
	username := "partybot"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.GetUser(userID, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if user.Data.Username != username {
		t.Errorf("User appears incorrect, got: %s, want: %s", user.Data.Username, username)
	}
	time.Sleep(Delay)
}

func TestGetUsers(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userIDs := []string{"1", "2", "3"}
	user1 := "33MHz"
	user2 := "Wife"
	user3 := "doctorlinguist"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	users, err := client.GetUsers(userIDs, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if users.Data[0].Username != user1 {
		t.Errorf("User appears incorrect, got: %s, want: %s", users.Data[0].Username, user1)
	}
	if users.Data[1].Username != user2 {
		t.Errorf("User appears incorrect, got: %s, want: %s", users.Data[1].Username, user2)
	}
	if users.Data[2].Username != user3 {
		t.Errorf("User appears incorrect, got: %s, want: %s", users.Data[2].Username, user3)
	}
	time.Sleep(Delay)
}

func TestReplaceProfile(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	name := "golang tester"
	profile := `{"name":"golang tester","content":{"text":"golang test profile"},"timezone":"America/Los_Angeles","locale":"en_US"}`
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.ReplaceProfile(profile)
	if err != nil {
		t.Error(err)
	}
	if user.Data.Name != name {
		t.Errorf("User appears incorrect, got: %s, want: %s", user.Data.Name, name)
	}
	time.Sleep(Delay)
}

func TestUpdateProfile(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	name := "golang test dude"
	profile := `{"name":"golang test dude"}`
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.UpdateProfile(profile)
	if err != nil {
		t.Error(err)
	}
	if user.Data.Name != name {
		t.Errorf("User appears incorrect, got: %s, want: %s", user.Data.Name, name)
	}
	time.Sleep(Delay)
}

func TestGetAvatarURL(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "145"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	res, err := client.GetAvatarURL(userID, url.Values{})
	if err != nil {
		t.Error(err)
	}
	t.Log(res)
	time.Sleep(Delay)
}

func TestUploadAvatar(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	fileName := "avatar.jpeg"
	filereader, err := os.Open(fileName)
	if err != nil {
		t.Error(err)
	}
	defer filereader.Close()
	params := map[string]string{
		"name":     fileName,
		"mimetype": "image/jpeg",
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.UploadAvatar(params, filereader)
	if err != nil {
		t.Error(err)
	}
	t.Logf(user.Data.Content.AvatarImage.URL)
	time.Sleep(Delay)
}

func TestGetCoverURL(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "145"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	res, err := client.GetCoverURL(userID, url.Values{})
	if err != nil {
		t.Error(err)
	}
	t.Log(res)
	time.Sleep(Delay)
}

func TestUploadCover(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	fileName := "cover.jpg"
	filereader, err := os.Open(fileName)
	if err != nil {
		t.Error(err)
	}
	defer filereader.Close()
	params := map[string]string{
		"name":     fileName,
		"mimetype": "image/jpeg",
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.UploadCover(params, filereader)
	if err != nil {
		t.Error(err)
	}
	t.Logf(user.Data.Content.CoverImage.URL)
	time.Sleep(Delay)
}

func TestGetFollowing(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "145"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	users, err := client.GetFollowing(userID, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if len(users.Data) < 1 {
		t.Errorf("I didn't get any following? count: %d", len(users.Data))
	}
	time.Sleep(Delay)
}

func TestGetFollowers(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "145"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	users, err := client.GetFollowers(userID, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if len(users.Data) < 1 {
		t.Errorf("I didn't get any followers? count: %d", len(users.Data))
	}
	time.Sleep(Delay)
}

func TestFollow(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "9"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.Follow(userID)
	if err != nil {
		t.Error(err)
	}
	if user.Data.YouFollow != true {
		t.Errorf("YouFollow is still set to false.")
	}
	time.Sleep(Delay)
}

func TestUnFollow(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "9"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.UnFollow(userID)
	if err != nil {
		t.Error(err)
	}
	if user.Data.YouFollow != false {
		t.Errorf("YouFollow is still set to true.")
	}
	time.Sleep(Delay)
}

func TestGetMuted(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	users, err := client.GetMuted(url.Values{})
	if err != nil {
		t.Error(err)
	}
	t.Log(users.Meta)
	time.Sleep(Delay)
}

func TestMute(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "494"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.Mute(userID)
	if err != nil {
		t.Error(err)
	}
	if user.Data.YouMuted != true {
		t.Errorf("YouMuted is still set to false.")
	}
	time.Sleep(Delay)
}

func TestUnMute(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "494"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.UnMute(userID)
	if err != nil {
		t.Error(err)
	}
	if user.Data.YouMuted != false {
		t.Errorf("YouMuted is still set to true.")
	}
	time.Sleep(Delay)
}

func TestGetBlocked(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	users, err := client.GetBlocked(url.Values{})
	if err != nil {
		t.Error(err)
	}
	t.Log(users.Meta)
	time.Sleep(Delay)
}

func TestBlock(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "494"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.Block(userID)
	if err != nil {
		t.Error(err)
	}
	if user.Data.YouBlocked != true {
		t.Errorf("YouBlocked is still set to false. %t", user.Data.YouBlocked)
	}
	time.Sleep(Delay)
}

func TestUnBlock(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "494"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	user, err := client.UnBlock(userID)
	if err != nil {
		t.Error(err)
	}
	if user.Data.YouBlocked != false {
		t.Errorf("YouBlocked is still set to true.")
	}
	time.Sleep(Delay)
}

func TestGetPresence(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "145"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	presence, err := client.GetPresence(userID)
	if err != nil {
		t.Error(err)
	}
	t.Log(presence.Meta)
	time.Sleep(Delay)
}

func TestSetPresence(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	text := "testing"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	presence, err := client.SetPresence(text)
	if err != nil {
		t.Error(err)
	}
	if presence.Data.Presence != text {
		t.Errorf("Unexpected result, got: %s, want: %s", presence.Data.Presence, text)
	}
	time.Sleep(Delay)
}

func TestGetMentions(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "145"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	posts, err := client.GetMentions(userID, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if len(posts.Data) < 1 {
		t.Error(posts)
	}
	time.Sleep(Delay)
}

func TestGetPostsFromUser(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "145"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	posts, err := client.GetPostsFromUser(userID, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if len(posts.Data) < 1 {
		t.Error(posts)
	}
	time.Sleep(Delay)
}

func TestGetBookmarks(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "145"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	posts, err := client.GetBookmarks(userID, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if len(posts.Data) < 1 {
		t.Error(posts)
	}
	time.Sleep(Delay)
}

func TestGetActionsForMe(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	interactions, err := client.GetActionsForMe(url.Values{})
	if err != nil {
		t.Error(err)
	}
	if len(interactions.Data) < 1 {
		t.Error(interactions)
	}
	time.Sleep(Delay)
}

func TestGetChannelsByMe(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channels, err := client.GetChannelsByMe(url.Values{})
	if err != nil {
		t.Error(err)
	}
	t.Log(channels.Meta)
	time.Sleep(Delay)
}

func TestGetNumberOfUnreadChannels(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("channel_types", "io.pnut.core.pm")
	res, err := client.GetNumberOfUnreadChannels(params)
	if err != nil {
		t.Error(err)
	}
	t.Log(res.Meta)
	time.Sleep(Delay)
}

func TestMarkReadChannels(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channeltypes := []string{"io.pnut.core.pm"}
	res, err := client.MarkReadChannels(channeltypes)
	if err != nil {
		t.Error(err)
	}
	t.Log(res.Meta)
	time.Sleep(Delay)
}

func TestGetSubscribedChannels(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channels, err := client.GetSubscribedChannels(url.Values{})
	if err != nil {
		t.Error(err)
	}
	t.Log(channels.Meta)
	time.Sleep(Delay)
}

func TestGetMutedChannels(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	channels, err := client.GetMutedChannels(url.Values{})
	if err != nil {
		t.Error(err)
	}
	t.Log(channels.Meta)
	time.Sleep(Delay)
}

func TestGetMessagesByMe(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	messages, err := client.GetMessagesByMe(url.Values{})
	if err != nil {
		t.Error(err)
	}
	t.Log(messages.Meta)
	time.Sleep(Delay)
}

func TestGetClients(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	userID := "145"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	clients, err := client.GetClients(userID)
	if err != nil {
		t.Error(err)
	}
	t.Log(clients.Meta)
	time.Sleep(Delay)
}
