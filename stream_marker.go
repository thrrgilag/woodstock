package woodstock

// MarkersResult response object
type MarkersResult struct {
	*CommonResponse
	Data []Marker `json:"data"`
}

// SetMarker sets the current read marker
// this func will be updated
// https://pnut.io/docs/resources/stream-marker
// https://pnut.io/docs/resources/stream-marker#post-markers
func (c *Client) SetMarker(json string) (result MarkersResult, err error) {
	responseCh := make(chan response)
	c.queryQueue <- query{url: MarkerAPI, data: &result, method: "POST", responseCh: responseCh, json: json}
	return result, (<-responseCh).err
}
