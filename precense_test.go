package woodstock

import (
	"testing"
	"time"
)

func TestGetPresences(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	res, err := client.GetPresences()
	if err != nil {
		t.Error(err)
	}
	t.Log(res.Meta)
	time.Sleep(Delay)
}
