package woodstock

import (
	"testing"
	"time"
)

func TestSetMarker(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	marker := `[{"id":"106930","name":"channel:1001"}]`
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	res, err := client.SetMarker(marker)
	if err != nil {
		t.Error(err)
	}
	t.Log(res.Meta)
	time.Sleep(Delay)
}
