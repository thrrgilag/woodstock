package woodstock

// Image object definition
type Image struct {
	URL       string `json:"url"`
	IsDefault bool   `json:"is_default"`
	Width     int    `json:"width"`
	Height    int    `json:"height"`
}

// ContentOfUser object definition
type ContentOfUser struct {
	Text         string   `json:"text"`
	HTML         string   `json:"html"`
	MarkdownText string   `json:"markdown_text"`
	Entities     Entities `json:"entities"`
	AvatarImage  Image    `json:"avatar_image"`
	CoverImage   Image    `json:"cover_image"`
}

// CountsOfUser object definition
type CountsOfUser struct {
	Bookmarks int `json:"bookmarks"`
	Clients   int `json:"clients"`
	Followers int `json:"followers"`
	Following int `json:"following"`
	Posts     int `json:"posts"`
}

// Verified object definition
type Verified struct {
	Domain string `json:"domain"`
	URL    string `json:"url"`
}

// User object definition
// https://pnut.io/docs/resources/users
type User struct {
	Badge        Badge                    `json:"badge"`
	Content      ContentOfUser            `json:"content"`
	Counts       CountsOfUser             `json:"counts"`
	CreatedAt    string                   `json:"created_at"`
	FollowsYou   bool                     `json:"follows_you"`
	ID           string                   `json:"id"`
	Locale       string                   `json:"locale"`
	Name         string                   `json:"name"`
	Timezone     string                   `json:"timezone"`
	Type         string                   `json:"type"`
	Username     string                   `json:"username"`
	YouBlocked   bool                     `json:"you_blocked"`
	YouCanFollow bool                     `json:"you_can_follow"`
	YouFollow    bool                     `json:"you_follow"`
	YouMuted     bool                     `json:"you_muted"`
	Verified     Verified                 `json:"verified"`
	Presence     string                   `json:"presence"`
	Raw          map[string][]interface{} `json:"raw"`
}

// Badge object definition
// https://pnut.io/docs/resources/users
type Badge struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Source object definition
type Source struct {
	Name string `json:"name"`
	URL  string `json:"url"`
	ID   string `json:"id"`
}

// CountsOfPost object definition
type CountsOfPost struct {
	Bookmarks int `json:"bookmarks"`
	Replies   int `json:"replies"`
	Reposts   int `json:"reposts"`
	Threads   int `json:"threads"`
}

// ContentOfPost object definition
type ContentOfPost struct {
	Text     string   `json:"text"`
	HTML     string   `json:"html"`
	Entities Entities `json:"entities"`
}

// Post object definition
// https://pnut.io/docs/resources/posts
type Post struct {
	BookmarkedBy  []User                   `json:"bookmarked_by"`
	Content       ContentOfPost            `json:"content"`
	Counts        CountsOfPost             `json:"counts"`
	CreatedAt     string                   `json:"created_at"`
	ID            string                   `json:"id"`
	IsDeleted     bool                     `json:"is_deleted"`
	IsNsfw        bool                     `json:"is_nsfw"`
	IsRevised     bool                     `json:"is_revised"`
	Raw           map[string][]interface{} `json:"raw"`
	ReplyTo       string                   `json:"reply_to"`
	RepostOf      *Post                    `json:"repost_of"`
	ReposedBy     []User                   `json:"reposed_by"`
	Revision      int                      `json:"revision"`
	Source        Source                   `json:"source"`
	ThreadID      string                   `json:"thread_id"`
	User          User                     `json:"user"`
	UserID        string                   `json:"user_id"`
	YouBookmarked bool                     `json:"you_bookmarked"`
	YouReposted   bool                     `json:"you_reposted"`
}

// Action object definition
type Action struct {
	PaginationID string `json:"pagination_id"`
	EventDate    string `json:"event_date"`
	Action       string `json:"action"`
	Users        []User `json:"users"`
	Objects      []Post `json:"objects"`
}

// Presence object definition
type Presence struct {
	ID         string `json:"id"`
	LastSeenAt string `json:"last_seen_at"`
	Presence   string `json:"presence"`
}

// Full object definition
type Full struct {
	Immutable bool     `json:"immutable"`
	You       bool     `json:"you"`
	UserIds   []string `json:"user_ids"`
	Users     []User   `json:"users"`
}

// Write object definition
type Write struct {
	*Full
	AnyUser bool `json:"any_user"`
}

// Read object definition
type Read struct {
	*Write
	Public bool `json:"public"`
}

// ACL object definition
type ACL struct {
	Full  Full  `json:"full"`
	Write Write `json:"write"`
	Read  Read  `json:"read"`
}

// CountsOfChannel object definition
type CountsOfChannel struct {
	Messages    int `json:"messages"`
	Subscribers int `json:"subscribers"`
}

// Channel object definition
// https://pnut.io/docs/resources/channels
type Channel struct {
	ACL                    ACL                      `json:"acl"`
	Counts                 CountsOfChannel          `json:"counts"`
	CreatedAt              string                   `json:"created_at"`
	HasStickyMessages      bool                     `json:"has_sticky_messages"`
	HasUnread              bool                     `json:"has_unread"`
	ID                     string                   `json:"id"`
	IsActive               bool                     `json:"is_active"`
	Raw                    map[string][]interface{} `json:"raw"`
	RecentDeletedMessage   Message                  `json:"recent_deleted_message"`
	RecentDeletedMessageID string                   `json:"recent_deleted_message_id"`
	RecentMessage          Message                  `json:"recent_message"`
	RecentMessageID        string                   `json:"recent_message_id"`
	Type                   string                   `json:"type"`
	YouMuted               bool                     `json:"you_muted"`
	YouSubscribed          bool                     `json:"you_subscribed"`
	User                   User                     `json:"user"`
	UserID                 string                   `json:"user_id"`
}

// CountsOfMessage object definition
type CountsOfMessage struct {
	Replies int `json:"replies"`
}

// ContentOfMessage object definition
type ContentOfMessage struct {
	HTML     string   `json:"html"`
	Text     string   `json:"text"`
	Entities Entities `json:"entities"`
}

// Message object definition
// https://pnut.io/docs/resources/messages
type Message struct {
	ChannelID string                   `json:"channel_id"`
	Counts    CountsOfMessage          `json:"counts"`
	CreatedAt string                   `json:"created_at"`
	DeletedBy string                   `json:"deleted_by"`
	ID        string                   `json:"id"`
	IsDeleted bool                     `json:"is_deleted"`
	IsSticky  bool                     `json:"is_sticky"`
	Source    Source                   `json:"source"`
	ReplyTo   string                   `json:"reply_to"`
	ThreadID  string                   `json:"thread_id"`
	User      User                     `json:"user"`
	UserID    string                   `json:"user_id"`
	Content   ContentOfMessage         `json:"content"`
	Raw       map[string][]interface{} `json:"raw"`
}

// AudioInfo object definition
type AudioInfo struct {
	Duration       int    `json:"duration"`
	DurationString string `json:"duration_string"`
	Bitrate        int    `json:"bitrate"`
}

// VideoInfo object definition
type VideoInfo struct {
	Duration       int    `json:"duration"`
	DurationString string `json:"duration_string"`
	Bitrate        int    `json:"bitrate"`
	Height         int    `json:"height"`
	Width          int    `json:"width"`
}

// UploadParameters object definition
type UploadParameters struct {
	Method string `json:"method"`
	URL    string `json:"url"`
}

// File object definition
// https://pnut.io/docs/resources/files
type File struct {
	AttachedTo       AttachedTo               `json:"attached_to"`
	AudioInfo        AudioInfo                `json:"audio_info"`
	CreatedAt        string                   `json:"created_at"`
	DerivedFiles     interface{}              `json:"derived_files"`
	FileToken        string                   `json:"file_token"`
	FileTokenRead    string                   `json:"file_token_read"`
	ID               string                   `json:"id"`
	ImageInfo        Image                    `json:"image_info"`
	IsComplete       bool                     `json:"is_complete"`
	IsPublic         bool                     `json:"is_public"`
	Kind             string                   `json:"kind"`
	MimeType         string                   `json:"mime_type"`
	Name             string                   `json:"name"`
	Raw              map[string][]interface{} `json:"raw"`
	Sha256           string                   `json:"sha256"`
	Size             int                      `json:"size"`
	Source           Source                   `json:"source"`
	TotalSize        int                      `json:"total_size"`
	Type             string                   `json:"type"`
	User             User                     `json:"user"`
	UserID           string                   `json:"user_id"`
	URL              string                   `json:"url"`
	URLExpiresAt     string                   `json:"url_expires_at"`
	URLShort         string                   `json:"url_short"`
	UploadParameters UploadParameters         `json:"upload_parameters"`
	VideoInfo        VideoInfo                `json:"video_info"`
}

// AttachedTo object definition
type AttachedTo struct {
	Messages []string `json:"messages"`
	Polls    []string `json:"polls"`
	Posts    []string `json:"posts"`
}

// ContentOfClient object definition
type ContentOfClient struct {
	*ContentOfMessage
}

// ClientInfo object definition
// https://pnut.io/docs/resources/clients
type ClientInfo struct {
	CreatedAt string          `json:"created_at"`
	User      User            `json:"created_by"`
	ID        string          `json:"id"`
	URL       string          `json:"url"`
	LogoImage string          `json:"logo_image"`
	Name      string          `json:"name"`
	Posts     int             `json:"posts"`
	Content   ContentOfClient `json:"content"`
}

// Marker object definition
type Marker struct {
	ID         string `json:"id"`
	LastReadID string `json:"last_read_id"`
	Percentage int    `json:"percentage"`
	UpdatedAt  string `json:"updated_at"`
	Version    string `json:"version"`
	Name       string `json:"name"`
}

// Poll object definition
// https://pnut.io/docs/resources/polls
type Poll struct {
	ClosedAt    string                   `json:"closed_at"`
	CreatedAt   string                   `json:"created_at"`
	ID          string                   `json:"id"`
	IsAnonymous bool                     `json:"is_anonymous"`
	IsPublic    bool                     `json:"is_public"`
	MaxOptions  int                      `json:"max_options"`
	Options     []PollOption             `json:"options"`
	PollToken   string                   `json:"poll_token"`
	Prompt      string                   `json:"prompt"`
	Source      Source                   `json:"source"`
	Type        string                   `json:"type"`
	User        User                     `json:"user"`
	UserID      string                   `json:"user_id"`
	Raw         map[string][]interface{} `json:"raw"`
}

// PollOption definition
type PollOption struct {
	Text           string   `json:"text"`
	Position       int      `json:"position"`
	IsYourResponse bool     `json:"is_your_response"`
	Respondents    int      `json:"respondents"`
	RespondentIDS  []string `json:"repondent_ids"`
}
