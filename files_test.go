package woodstock

import (
	"net/url"
	"os"
	"testing"
	"time"
)

func TestGetFile(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	fileID := "15596"
	fileName := "hawkward.jpg"
	client := NewClient(config.ClientID, "")
	// client.SetAccessToken(config.AccessToken)
	file, err := client.GetFile(fileID, url.Values{})
	if err != nil {
		t.Error(err)
	}
	if file.Data.Name != fileName {
		t.Errorf("File appears incorrect, got: %s, want: %s", file.Data.Name, fileName)
	}
	t.Log(file.Meta)
	time.Sleep(Delay)
}

func TestGetFiles(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	// ids := []string{"15589", "15596", "15597"}
	fileName1 := "test-pattern.jpg"
	fileName2 := "hawkward.jpg"
	fileName3 := "walken.jpg"
	client := NewClient(config.ClientID, "")
	// client.SetAccessToken(config.AccessToken)
	params := url.Values{}
	params.Set("ids", "15589,15596,15597")
	files, err := client.GetFiles(params)
	if err != nil {
		t.Error(err)
	}
	if files.Data[0].Name != fileName1 {
		t.Errorf("File appears incorrect, got: %s, want: %s", files.Data[0].Name, fileName1)
	}
	if files.Data[1].Name != fileName2 {
		t.Errorf("File appears incorrect, got: %s, want: %s", files.Data[1].Name, fileName2)
	}
	if files.Data[2].Name != fileName3 {
		t.Errorf("File appears incorrect, got: %s, want: %s", files.Data[2].Name, fileName3)
	}
	time.Sleep(Delay)
}

func TestGetUserFiles(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	files, err := client.GetUserFiles(url.Values{})
	if err != nil {
		t.Error(err)
	}
	if len(files.Data) < 3 {
		t.Errorf("I recieved fewer files than expected, got: %d, want at least: %d", len(files.Data), 3)
	}
	time.Sleep(Delay)
}

func TestCreateFile(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	filename := "test-pattern.jpg"
	filereader, err := os.Open(filename)
	if err != nil {
		t.Error(err)
	}
	defer filereader.Close()
	params := map[string]string{
		"type":      "dev.thrrgilag.woodstock",
		"name":      filename,
		"kind":      "image",
		"mimetype":  "image/jpeg",
		"is_public": "true",
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	file, err := client.CreateFile(params, filereader)
	if err != nil {
		t.Error(err)
	}
	if file.Data.Name != filename {
		t.Errorf("File appears incorrect, got: %s, want: %s", file.Data.Name, filename)
	}
	time.Sleep(Delay)
}

func TestCreateFilePlaceholder(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	var params File
	params.Type = "dev.thrrgilag.woodstock"
	params.Name = "test-pattern.jpg"
	params.Kind = "image"
	params.MimeType = "image/jpeg"
	params.IsPublic = true
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	file, err := client.CreateFilePlaceholder(params)
	if err != nil {
		t.Error(err)
	}
	t.Log(file.Meta)
	time.Sleep(Delay)
}

// TODO: is broken
// func TestUpdateFileContent(t *testing.T) {
// 	config, err := GetConfig()
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	filename := "test-pattern.jpg"
// 	filereader, err := os.Open(filename)
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	defer filereader.Close()
// 	client := NewClient(config.ClientID, "")
// 	client.SetAccessToken(config.AccessToken)
// 	file, err := client.UpdateFileContent("15588", filereader)
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	t.Log(file.Meta)
// 	time.Sleep(Delay)
// }

func TestUpdateFileDetails(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	fileID := "14987"
	// name := "test image"
	var details FileDetails
	details.Name = "test_image"
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	file, err := client.UpdateFileDetails(fileID, details)
	if err != nil {
		t.Error(err)
	}
	t.Log(file.Meta)
	time.Sleep(Delay)
	// TODO: change filename string to timestamp that can be checked
}

func TestDeleteFile(t *testing.T) {
	config, err := GetConfig()
	if err != nil {
		t.Error(err)
	}
	filename := "test-pattern.jpg"
	filereader, err := os.Open(filename)
	if err != nil {
		t.Error(err)
	}
	defer filereader.Close()
	params := map[string]string{
		"type":      "dev.thrrgilag.woodstock",
		"name":      filename,
		"kind":      "image",
		"mimetype":  "image/jpeg",
		"is_public": "true",
	}
	client := NewClient(config.ClientID, "")
	client.SetAccessToken(config.AccessToken)
	newfile, err := client.CreateFile(params, filereader)
	if err != nil {
		t.Error(err)
	}
	file, err := client.DeleteFile(newfile.Data.ID)
	if err != nil {
		t.Error(err)
	}
	t.Log(file.Meta)
	time.Sleep(Delay)
}
